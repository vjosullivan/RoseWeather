//
//  WeatherConditions.swift
//  RoseWeather
//
//  Created by Vincent O'Sullivan on 15/08/2016.
//  Copyright © 2016 Vincent O'Sullivan. All rights reserved.
//

import Foundation

public enum PressureTrend: String {
    case rising
    case falling
    case steady
}

public struct WeatherConditions {
    
    public let timestamp: Date

    public let latitude:  Measurement<UnitAngle>?
    public let longitude: Measurement<UnitAngle>?
    public let elevation: Measurement<UnitLength>?
    public let locationName: String?

    public let temperature:   Measurement<UnitTemperature>?
    public let feelsLike:     Measurement<UnitTemperature>?
    
    public let windSpeed:     Measurement<UnitSpeed>?
    public let windGustSpeed: Measurement<UnitSpeed>?
    public let windDirection: Measurement<UnitAngle>?
    public let windDescription: String?

    public let pressure:      Measurement<UnitPressure>?
    public let pressureTrend: PressureTrend?

    public let shortDescription: String?
    public let iconName: String?
    
    public let visibility: Measurement<UnitLength>?
    public let uvIndex: Double?
}

extension WeatherConditions: CustomStringConvertible {
    
    public var description: String {
        var desc = "Current observation:"
        desc += "\n   Timestamp: \(timestamp.description(with: Locale.current))."
        desc += "\n   Location: \(locationName ?? "Unknown")."
        desc += "\n   Location: (\(latitude?.description ?? "Unknown"), \(longitude?.description ?? "Unknown")), \(elevation?.description ?? "Unknown")."
        desc += "\n   Short description:    \(shortDescription ?? "unknown") (use icon '\(iconName ?? "unknown")')."
        desc += "\n   Temperature: \(temperature?.description ?? "Unknown") (feels like \(feelsLike?.description ?? "unknown"))."
        desc += "\n   Wind:        \(windDirection?.description ?? "Unknown direction") at \(windSpeed?.description ?? "Unknown") (gusting to \(windGustSpeed?.description ?? "Unknown")).   \(windDescription ?? "")."
        desc += "\n   Pressure:    \(pressure?.description ?? "unknown") (\(pressureTrend?.rawValue ?? ""))."
        desc += "\n   Visibility:  \(visibility?.description ?? "unknown")."
        desc += "\n   UV Index:    \(uvIndex?.description ?? "unknown")."
        return desc
    }
}
