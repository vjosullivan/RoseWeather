//
//  WeatherClient.swift
//  RoseWeather
//
//  Created by Vincent O'Sullivan on 10/08/2016.
//  Copyright © 2016 Vincent O'Sullivan. All rights reserved.
//

import Foundation

public typealias WeatherCompletionHandler = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void

public enum Location {
    case coordinates(latitude: Double, longitude: Double)
}

/// Handles all communication (to and) from the supplied URL.
public class WeatherClient {

    private static let defaultURL = "http://api.wunderground.com/api/c248b37c31384331/almanac/q/"

    private let url: URL

    public convenience init(location: Location) {
        let initialURL: URL
        switch location {
        case .coordinates(let lat, let long):
            initialURL = URL(string: WeatherClient.defaultURL + String(lat) + "," + String(long) + ".json")!
        }
        self.init(url: initialURL)
    }

    public convenience init() {
        self.init(location: .coordinates(latitude: 51.432, longitude: -1.0022))
    }

    public init(url: URL) {
        self.url = url
    }

    /// Fetches data from a predefined URL and sends the results to the handler.
    ///
    /// - parameter completionHandler: The closure to be executed on completion of the fetch.
    ///
    public func fetchData(completionHandler handler: WeatherCompletionHandler) {
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: handler)
        task.resume()
    }
}
