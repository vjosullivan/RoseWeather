//
//  WeatherBuilder.swift
//  RoseWeather
//
//  Created by Vincent O'Sullivan on 15/08/2016.
//  Copyright © 2016 Vincent O'Sullivan. All rights reserved.
//

import Foundation

public enum WeatherBuilderError: Error {
    case unableToParseData
    case invalidObservationData
    case invalidTimestamp
}

public class WeatherBuilder {
    
    public func parseConditions(data: Data) throws -> WeatherConditions? {
        var conditions: WeatherConditions?
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            if let current = json["current_observation"] as? [String: AnyObject],
                let unixTime = Double(current["local_epoch"] as! String) {
                let timestamp = Date(timeIntervalSince1970: unixTime)
                
                conditions = WeatherConditions(
                    timestamp: timestamp,
                    latitude:   measurement(value: current["display_location"]?["latitude"],  unit: UnitAngle.degrees),
                    longitude:  measurement(value: current["display_location"]?["longitude"], unit: UnitAngle.degrees),
                    elevation:  measurement(value: current["display_location"]?["elevation"], unit: UnitLength.meters),
                    locationName: current["display_location"]?["full"] as? String,
                    temperature: measurement(value: current["temp_c"],      unit: UnitTemperature.celsius),
                    feelsLike:   measurement(value: current["feelslike_c"], unit: UnitTemperature.celsius),
                    windSpeed:     measurement(value: current["wind_kph"],      unit: UnitSpeed.kilometersPerHour),
                    windGustSpeed: measurement(value: current["wind_gust_kph"], unit: UnitSpeed.kilometersPerHour),
                    windDirection: measurement(value: current["wind_degrees"],  unit: UnitAngle.degrees),
                    windDescription: current["wind_string"] as? String,
                    pressure:      measurement(value: current["pressure_mb"],   unit: UnitPressure.millibars),
                    pressureTrend: pressureTrend(from: current["pressure_trend"]),
                    shortDescription: current["weather"] as? String,
                    iconName: current["icon"] as? String,
                    visibility:  measurement(value: current["visibility_km"], unit: UnitLength.kilometers),
                    uvIndex: Double(current["UV"] as? String ?? ""))
                print("\n", conditions!, "\n")
            } else {
                throw WeatherBuilderError.invalidObservationData
            }
        } catch {
            throw WeatherBuilderError.unableToParseData
        }
        return conditions
    }

    /// Returns an (optional) `Measurement` instance if the given value is either a `Double`
    /// or a `String` containing a `Double`, otherwise `nil`.
    ///
    /// - parameter value: Hopefully, something that can be converted to a `Double`.
    /// - parameter unit: A `Measurement.Unit`.
    /// - returns: A `Measurement` if the input can be decoded, otherwise `nil`.
    internal func measurement<U>(value: Any?, unit: U) -> Measurement<U>? {
        if let value = value as? Double {
            return Measurement(value: value, unit: unit)
        }
        if let value = Double((value as? String) ?? "") {
            return Measurement(value: value, unit: unit)
        }
        return nil
    }

    /// Converts a `String` to a `PressureTrend` enum.
    ///
    /// - parameter from: The `String` to be converted.
    /// - returns: A `PressureTrend` if the input can be decoded, otherwise `nil`.
    internal func pressureTrend(from value: Any?) -> PressureTrend? {
        guard let value = value as? String else {
            return nil
        }
        let trend: PressureTrend?
        switch value {
        case "0":
            trend = .steady
        case "+":
            trend = .rising
        case "-":
            trend = .falling
        default:
            trend = nil
        }
        return trend
    }
}
