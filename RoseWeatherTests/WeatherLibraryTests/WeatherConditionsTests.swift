//
//  WeatherConditionsTests.swift
//  RoseWeather
//
//  Created by Vincent O'Sullivan on 15/08/2016.
//  Copyright © 2016 Vincent O'Sullivan. All rights reserved.
//

import XCTest
@testable import RoseWeather

class WeatherConditionsTests: XCTestCase {
    
    func testCreatable() {
        XCTAssertNotNil(WeatherConditions(
            timestamp: Date(),
            latitude: nil,
            longitude: nil,
            elevation: nil,
            locationName: nil,
            temperature: nil,
            feelsLike: nil,
            windSpeed: nil,
            windGustSpeed: nil,
            windDirection: nil,
            windDescription: nil,
            pressure: nil,
            pressureTrend: nil,
            shortDescription: nil,
            iconName: nil,
            visibility: nil,
            uvIndex: nil))
    }
}
