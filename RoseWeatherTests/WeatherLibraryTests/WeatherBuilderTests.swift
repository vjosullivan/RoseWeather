//
//  WeatherBuilderTests.swift
//  RoseWeather
//
//  Created by Vincent O'Sullivan on 15/08/2016.
//  Copyright © 2016 Vincent O'Sullivan. All rights reserved.
//

import XCTest
@testable import RoseWeather

class WeatherBuilderTests: XCTestCase {

    func testCreatable() {
        let wb = WeatherBuilder()
        XCTAssertNotNil(wb)
    }
    
    func testParseInvalidConditions() {
        let data = fetchTestData(path: "/simple.json")
        let wb   = WeatherBuilder()
        XCTAssertThrowsError(try wb.parseConditions(data: data))
    }
    
    func testParseValidConditionsData() {
        let data = fetchTestData(path: "/conditions.json")
        let wb   = WeatherBuilder()
        let conditions = try! wb.parseConditions(data: data)
        XCTAssertNotNil(conditions)
    }
    
    /// Test valid data with some missing components.
    func testParseMissingConditionsData() {
        let data = fetchTestData(path: "/missingConditions.json")
        let wb   = WeatherBuilder()
        let conditions = try! wb.parseConditions(data: data)
        XCTAssertNotNil(conditions)
    }
    
    func testPressureTrend() {
        let wb   = WeatherBuilder()
        XCTAssertEqual(PressureTrend.steady,  wb.pressureTrend(from: "0"))
        XCTAssertEqual(PressureTrend.rising,  wb.pressureTrend(from: "+"))
        XCTAssertEqual(PressureTrend.falling, wb.pressureTrend(from: "-"))
        XCTAssertEqual(nil, wb.pressureTrend(from: "X"))
        XCTAssertEqual(nil, wb.pressureTrend(from: nil))
    }
    
    //////////////
    
    private func fetchTestData(path: String) -> Data {
        let fileURL  = URL(fileURLWithPath: Bundle(for: type(of: self)).bundlePath.appending(path))
        return try! Data(contentsOf: fileURL)
    }
}
