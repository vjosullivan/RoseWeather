//
//  WeatherClientTests.swift
//  RoseWeather
//
//  Created by Vincent O'Sullivan on 10/08/2016.
//  Copyright © 2016 Vincent O'Sullivan. All rights reserved.
//

import XCTest
@testable import RoseWeather

class WeatherClientTests: XCTestCase {

    var callCount = 0

    override func setUp() {
        callCount = 0
    }
    
    func testCreatable() {
        let dummyURL = URL(string: "anything")!
        let wc = WeatherClient(url: dummyURL)
        XCTAssertNotNil(wc, "A WeatherClient must be creatable.")
    }

    /// Reading the default URL should return data.
    func testDefaultURL() {

        let exp = expectation(description: "Default URL.")

        let wc = WeatherClient()
        wc.fetchData(completionHandler:  {(data, response, error) in
            self.assertDataFound(data, response, error)
            self.callCount += 1
            exp.fulfill()
        })

        assertCompletionHandlerCalled()
    }

    /// Reading a missing URL should result in no data and an error.
    func testDefaultClient() {

        let exp = expectation(description: "Missing file.")

        let wc = createTestClient(testFile: "/missing.file")
        wc.fetchData(completionHandler:  {(data, response, error) in
            self.assertNoDataFound(data, response, error)
            self.callCount += 1
            exp.fulfill()
        })

        assertCompletionHandlerCalled()
    }

    /// Reading a valid URL containing junk should result in data but no JSON.
    func testCannotReadJunkJson() {

        let exp = expectation(description: "Junk JSON.")

        let wc = createTestClient(testFile: "/junk.json")
        wc.fetchData(completionHandler:  {(data, response, error) in
            self.assertDataFound(data, response, error)
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            XCTAssertNil(json, "Junk data should result in nil JSON.")

            self.callCount += 1
            exp.fulfill()
        })

        assertCompletionHandlerCalled()
    }

    /// Reading a valid URL containing "JSON-like" junk should result in data but no JSON.
    func testCannotReadInvalidJson() {

        let exp = expectation(description: "Invalid JSON.")

        let wc = createTestClient(testFile: "/invalid.json")
        wc.fetchData(completionHandler:  {(data, response, error) in
            self.assertDataFound(data, response, error)
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            XCTAssertNil(json, "Invalid json data should result in nil JSON.")

            self.callCount += 1
            exp.fulfill()
        })

        assertCompletionHandlerCalled()
    }

    /// Reading an valid URL which is empty should result in data but no JSON.
    func testCannotReadEmptyJson() {

        let exp = expectation(description: "Empty JSON.")

        let wc = createTestClient(testFile: "/empty.json")
        wc.fetchData(completionHandler:  {(data, response, error) in
            self.assertDataFound(data, response, error)
            let json = try? JSONSerialization.jsonObject(with: data!, options: [])
            XCTAssertNil(json, "Empty json data should result in nil JSON.")

            self.callCount += 1
            exp.fulfill()
        })

        assertCompletionHandlerCalled()
    }

    /// Reading valid data from a valid URL should result data
    /// that can be converted into a JSON string.
    func testCanReadJSONFromSimpleJson() {

        let exp = expectation(description: "Simple Read.")

        let wc = createTestClient(testFile: "/simple.json")
        wc.fetchData(completionHandler:  {(data, response, error) in
            self.assertDataFound(data, response, error)
            if let json = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String: AnyObject] {
                XCTAssertEqual("simple data", json["simpleKey"] as! String)
            } else {
                XCTAssertFalse(true, "Test should not fail.")
            }
            self.callCount += 1
            exp.fulfill()
        })

        assertCompletionHandlerCalled()
    }

    //////////////////////////////////////

    /// Assert that data has been found and no error thrown.
    private func assertDataFound(_ data: Data?, _ response: URLResponse?, _ error: Error?) {
        XCTAssertNotNil(data)
        XCTAssertNotNil(response)
        XCTAssertNil(error)
    }

    /// Assert that no data has been found and an error thrown.
    private func assertNoDataFound(_ data: Data?, _ response: URLResponse?, _ error: Error?) {
        XCTAssertNil(data)
        XCTAssertNil(response)
        XCTAssertNotNil(error)
        print("\nError type: \(type(of: error))\n")
    }

    /// Helper function / common code.
    private func assertCompletionHandlerCalled() {
        waitForExpectations(timeout: 10, handler: { (error) in
            XCTAssertNil(error)
            XCTAssertEqual(1, self.callCount, "Handler should have been called.")
        })
    }

    /// Helper function / common code.
    private func createTestClient(testFile: String) -> WeatherClient {
        let filePath = Bundle(for: type(of: self)).bundlePath.appending(testFile)
        let fileURL  = URL(fileURLWithPath: filePath)
        return WeatherClient(url: fileURL)
    }
}
